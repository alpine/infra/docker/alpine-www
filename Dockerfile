FROM registry.alpinelinux.org/img/alpine:edge

RUN apk add --no-cache \
    curl \
    darkhttpd \
    git \
    lua5.3 \
    lua-rapidjson \
    lua-discount \
    lua-feedparser \
    lua-filesystem \
    lua-lustache \
    lua-lyaml \
    make \
    mqtt-exec \
    rsync \
    && install -dm0755 -o darkhttpd -g www-data /src \
    && install -dm0755 -o darkhttpd -g www-data /site \
    && touch /site/.keep

COPY overlay /

USER darkhttpd

WORKDIR /src

ENTRYPOINT ["/usr/local/bin/entrypoint"]

CMD ["build"]
