# alpine-www in docker

This builds and hosts the Alpine Linux website in docker. It also listens to
updates from mqtt to keep it up-to-date.

## Usage

Add a `docker-compose.override.yml` if you want to publish a port. For example:

``` yaml
version: "3.7"
services:
  web:
    ports:
      - 8080:8080
```

Then, to run:

``` sh
docker-compose up -d
```

The website should be available on 'http://localhost:8080'

## Environment variables

| Variable      | Description                       | Default                                                          |
|---------------|-----------------------------------|------------------------------------------------------------------|
| REPOSITORY    | The git repository to fetch from  | https://gitlab.alpinelinux.org/alpine/infra/mk-site.git          |
| BRANCH        | The branch to track               | production                                                       |
| MQTT_HOST     | What MQTT host to listen to       | msg.alpinelinux.org                                              |
| MQTT_TOPIC    | What topic(s) to listen to        | gitlab/push/alpine/infra/alpine-mksite git/aports/master rsync/# |
| MQTT_USER     | The username to authenticate with | (optional)                                                       |
| MQTT_PASSWORD | The password to authenticate with | (required if username is specified)                              |
